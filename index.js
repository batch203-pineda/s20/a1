// console.log("Hello World!");

let numVar = function printNum () {
    let numInput = prompt("Give me a number");
    console.log("The number you provided is " +numInput);
    
    for (let i = numInput; i >= 0; i-= 5  ) {
        if (i <= 50){
            console.log("The current value is 50 at. Terminating the loop.");
            break;
        }else if(i % 10 === 0) {
            console.log("The number is divisible by 10. Skipping the number");
            continue;
        } else if(i % 5 === 0) {
            console.log(i);
            continue;
        }
        else{
            console.log(i);
        }
    }
}

numVar();


let word = "supercalifragilisticexpialidocious";
let consonants = "";

for (let i = 0; i < word.length; i++){
    
    	if( word[i].toLowerCase() === 'a' || 
    		word[i].toLowerCase() === 'e' || 
    		word[i].toLowerCase() === 'i' || 
    		word[i].toLowerCase() === 'o' || 
    		word[i].toLowerCase() === 'u') {
            //console.log("skipping vowel "+word[i]);
            continue;
    	} else {
            consonants += word[i];
        }    	
    }
    console.log(word);
    console.log(consonants);
